// const http = require('http');

const express = require('express'); //express is saved as a prod dependancy --save


//This will initialise an express app which you can then deploy in the app
const app = express();

// //middleware dployment with express app
// app.use((req, res, next) => {
//     console.log('In the first middleware');
//     next(); //allows the next middleware to take control of the request
// });
app.use('/add-product',(req, res, next) => {
    console.log('In the second middleware');
    res.send('<h1>You\'re on the products page</h1>'); // Send any response but deafult header is "text/html"
});


app.use('/',(req, res, next) => {
    console.log('In the second middleware');
    res.send('<h1>Hello World</h1>'); // Send any response but deafult header is "text/html"
});


app.listen(3000);

//Shelved in favour of the express 
// const server =http.createServer(app);
// server.listen(3000);