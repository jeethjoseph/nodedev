const requestHandler = (req, res) => {
    const url = req.url;
    const method = req.method;

    if (url === '/'){
        // console.log("/");
        res.write('<html><head><title>slash</title></head><body><form action="/create-user" method="POST"><h3>User Name</h3><input type="text" name="userName"><button type="submit">SUBMIT</button></form></body></html>');
        return res.end();
    }
    if (url === '/users'){
        // console.log("users");
        res.write('<html><head><title>slashUsers</title></head><body><ul><li>User 1</li></ul></body></html>');
        return res.end();
    }
    if (url === '/create-user' && method === 'POST'){
        // console.log("create-user");
        const body = [];
        req.on('data', (chunk) => {
            // console.log(chunk);
            body.push(chunk);


        });
        return req.on('end', () => {
            const parsedData = Buffer.concat(body).toString();
            const userName = parsedData.split('=')[1];
            console.log(userName);
            return res.end();

        })
    }
}

module.exports = {
    handler : requestHandler
}