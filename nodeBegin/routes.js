const fs = require('fs');

const requestHandler = (req, res) => {
     // console.log(req.url, req.method, req.headers);
    // process.exit();  //hard exits the event loop

    const url = req.url;
    const method = req.method;


    //filtering request by destination
    if (url === '/') {
        res.write('<html>');
        res.write('<head><title>Message</title></head>');
        res.write('<body>');
        res.write('<h1>Enter a Message</h1>');
        //form action dictates where the message goes. Destination URL. The POST request is made (not verified). input name message will be the default name for the input entered by the user
        res.write('<form action="/message" method="POST"><input type="text" name="message"><button type="submit">SEND</button></form>');

        res.write('</body>');
        res.write('</html>');
        return res.end();
    }

    //filtering request by destination / message and post method
    if (url === '/message' && method === 'POST') {
        // node streams data. breaks the in coming request into chunks 
        //chunks are worked with through buffers. Groups of data chunks to help the user manipulate data

        //event listener on listens for data event. Which is just saying a new chunk is getting ready to be read.And a callback function to decide what to do with the chunk. Same as event listeners in JS
        const body = [];
        req.on('data',(chunk)=>{
            // console.log(chunk);
            body.push(chunk);

        });
        //event listener for end of post recieving
        //Because the program runs asynchronously the next line have to be returned. otherwise the code after it is executed.

        return req.on('end', () => {
            //Buffer class global to node concats the array of chunks
            const parsedBody = Buffer.concat(body).toString();
            // console.log(parsedBody);
            //Parsed body is observed as key value pair. Chunk was totally unreadable
            const message = parsedBody.split('=');
            // console.log(message);
            const toWrite = message[1];

            //writing file sychronously will hold up the program. Alternatively write it asynchronously and call back
            // fs.writeFileSync('message.txt', toWrite);
            //Writing files is handled by a worker pool on the system. While node is on single thread js. Worker pool is a system function. Node listens for the call back from the wprker pool after it is done

            fs.writeFile('message.txt', toWrite, (err) => {
                //redirect is only activated after the completion of the request
                res.statusCode = 302;
                res.setHeader('Location', '/');
                return res.end();
            });
            
        });
        
        
    }

    res.setHeader('Content-Type', 'text/html');
    res.write('<html>');
    res.write('<head><title>My first page</title></head>');
    res.write('<body>');
    res.write('<h1>Hello World</h1>');
    res.write('</body>');
    res.write('</html>');
    res.end();

}


module.exports = {
    handler: requestHandler
};
   