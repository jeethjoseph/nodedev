//Event loop architecture
// First listens for timers setTimeout, setInterval
//Pending call backs > Poll for new call backs > setImmediate() > Close event call backs

const http = require('http');
//imported from routes modularity
const routes = require('./routes');



//create server call back
//Callbak is in routes.js
const server = http.createServer(routes.handler);

//this keeps the rode running. constantly listening for requests
server.listen(3000);